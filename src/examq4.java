import java.util.Scanner;
public class examq4 {
    public static void main(String args[]) {
        String[] words = new String[5];
        
        Scanner input = new Scanner(System.in);
        // Read user input 5 times  
        for(int i=0; i<words.length; i++) {
            System.out.println("Enter a word: ");
            words[i] = input.nextLine();
        }
        //prints out every word
        for(int i=0; i<words.length; i++) {
            for(int j=0; j<words.length; j++) {
                if(words[i].equals(words[j]) && i!=j) {
                    System.out.println("Word is repeated: "+words[i]);
                }
            }
        }        
        input.close();
    }
}