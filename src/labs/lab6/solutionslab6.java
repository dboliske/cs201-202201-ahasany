import java.util.*;

public class solutionslab6 {
    
    static Scanner input = new Scanner(System.in);
    
    public int addCustomer(ArrayList<String> listNames){
        System.out.print("Welcome to the deli! Your name is: ");
        input.nextLine();
        String name = input.nextLine();
        
        listNames.add(name);
        
        return listNames.size();
        
    }
    
    public void helpCustomer(ArrayList<String> listNames){
        
        String name = listNames.remove(0);
        
        System.out.println(name);
    }
    
    public void menu(ArrayList<String> listNames){
        
        int choice = 0;
        
        while(choice != 3){
            
            System.out.println(" 1. Add customer to queue \n 2. Help customer \n 3. Exit");
                System.out.println("Enter a command");
            
            choice = input.nextInt();
        
    switch (choice) {
        case 1:
            addCustomer(listNames);
            break;
        case 2:
            helpCustomer(listNames);
            break;
        default:
            System.out.println("Please enter a valid input");
        }

        }
    }
    
    
    
        public static void main(String[] args) {
            
            ArrayList<String> listNames = new ArrayList<String>();
            
        solutionslab6 deli = new solutionslab6();
        
        deli.menu(listNames);
        
    }
}