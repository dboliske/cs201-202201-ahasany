# Lab 1

## Total

10/20

## Break Down

* Exercise 1    2/2
* Exercise 2    1/2
* Exercise 3    2/2
* Exercise 4
  * Program     2/2
  * Test Plan   0/1
* Exercise 5
  * Program     1/2
  * Test Plan   0/1
* Exercise 6
  * Program     2/2
  * Test Plan   0/1
* Documentation 0/5

## Comments
- Each exercise should be its own Java class
- Make sure the project is working properly, answers_1.java is not working properly

- Exercise 2 has input parsing errors and is only working for the 3rd bullet point.
- Exercise 5 has plenty of syntax errors and is not working at all. (while loop condition never met, calculations are for inches while answer should be in square feet)
- No test plans or documentation were included