//1.Write a Java program that will prompt the user for the grades for an exam, 
	//computes the average, and returns the result. 
	//Your program must be able to handle an unspecified number of grades.
import java.util.Scanner;  // Import the Scanner class
import java.io.*;
import java.net.*;
public class lab2_1
{
	public static void main(String[] args) {
	    Scanner input = new Scanner(System.in);
	    int studentGrade = 0;
	    int i = 0;
	    double sum = 0;
	    double avg = 0;
	    while(studentGrade != -1) {
	        System.out.print("\nEnter the student's grade or [-1] to quit: ");
	        studentGrade = input.nextInt();
	        sum += studentGrade;
	        i++;
	        if(studentGrade == -1){
	            break;
	            }
	        }
	    avg = sum/i;
        System.out.println("Average grade was:  " + avg); 
	}
}