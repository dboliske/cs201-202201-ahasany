# Lab 2

## Total

13/20

## Break Down

* Exercise 1    5/6
* Exercise 2    4/6
* Exercise 3    4/6
* Documentation 0/2

## Comments
- Labs setup improperly on Eclipse
- Exercises are misnumbered
- Ex1. output doesn't match expected output (-1 point)
- Ex2. Program doesn't handle decimals (float or double) (-1 point)
- Ex2. Program outputting wrong average (it is including -1 in the grades) (-1 point)
- Ex3. Program isn't running repeatedly (-1 point)
- Ex3. Addition and Multiplication are not implementing decimals points (-1 point)
- No documentation