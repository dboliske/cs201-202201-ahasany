# Lab 4

## Total

20/30

## Break Down

* Part I GeoLocation
  * Exercise 1-9        6/8
  * Application Class   0/1
* Part II PhoneNumber
  * Exercise 1-9        6/8
  * Application Class   0/1
* Part III 
  * Exercise 1-8        5/8
  * Application Class   0/1
* Documentation         3/3

## Comments
- All classes are not valid .java files.
- Default constructor implemented wrong on all classes.
- Potion class has string defined outside of class scope.
- No application classes.