# Final Exam

## Total

75/100

## Break Down

1. Inheritance/Polymorphism:    19/20
    - Superclass:               5/5
    - Subclass:                 5/5
    - Variables:                5/5
    - Methods:                  4/5
2. Abstract Classes:            18/20
    - Superclass:               4/5
    - Subclasses:               5/5
    - Variables:                5/5
    - Methods:                  4/5
3. ArrayLists:                  18/20
    - Compiles:                 4/5
    - ArrayList:                5/5
    - Exits:                    5/5
    - Results:                  4/5
4. Sorting Algorithms:          20/20
    - Compiles:                 5/5
    - Selection Sort:           10/10
    - Results:                  5/5
5. Searching Algorithms:        0/20
    - Compiles:                 0/5
    - Jump Search:              0/10
    - Results:                  0/5

## Comments

1. In q1, seats must be always positive.
2. In q2, didn't use abstraction, didn't validate positive values.
3. In q3, inputting done returns an error, should only print once in the end.
4.
5.
