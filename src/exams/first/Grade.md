# Midterm Exam

## Total

84/100

## Break Down

1. Data Types:                  19/20
    - Compiles:                 4/5
    - Input:                    5/5
    - Data Types:               5/5
    - Results:                  5/5
2. Selection:                   15/20
    - Compiles:                 3/5
    - Selection Structure:      10/10
    - Results:                  2/5
3. Repetition:                  19/20
    - Compiles:                 4/5
    - Repetition Structure:     5/5
    - Returns:                  5/5
    - Formatting:               5/5
4. Arrays:                      19/20
    - Compiles:                 4/5
    - Array:                    5/5
    - Exit:                     5/5
    - Results:                  5/5
5. Objects:                     12/20
    - Variables:                4/5
    - Constructors:             3/5
    - Accessors and Mutators:   3/5
    - toString and equals:      2/5

## Comments

1. Program is not a Java file.
2. Program has multiple compiler errors, print statements are incorrectly written, and output is not correctly a String.
3. Program is not a Java file.
4. Program is not a Java file.
5. Program is not in the correct location, class is not name `Pet`, non-default constructor does not validate `age` before setting it, `setAge` does not validate age before setting it, the `equals` method does not follow the UML diagram, and the `equals` method does not correctly compare `name`.
