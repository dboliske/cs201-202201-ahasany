//Write a program that prompts the user for a String input 
//and then prints it back out to the console.

import java.util.Scanner;

public class DataTypesOne {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter a string:");

        String in = input.nextLine();

        System.out.println("You entered '" + in + "'");

        input.close();
    }
}