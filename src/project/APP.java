package project;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
//MAKE ARRAYLIST AND READFILE AND FILEWRITER
	class APP{
		public void main(String args[])
		  {
		     Scanner scan = new Scanner(System.in);
		     System.out.println("GROCERY STORE");
		     int choice = 0;
		     while(choice != 6)
		     {
		        System.out.println("Menu:");
		        System.out.println("1| Add Item");
		        System.out.println("2| Buy Item");
		        System.out.println("3| Search Item");
		        System.out.println("4| Modify Item");
		        System.out.println("5| View Inventory");
		        System.out.println("6| Exit");
		        System.out.println("Enter a number");
		        choice = scan.nextInt();
		        if(choice == 1)
		        {
		           System.out.println("What type of item is this?");
		           System.out.println("1| Produce");
		           System.out.println("2| Shelved");
		           System.out.println("3| Age Restricted");
		           int type = scan.nextInt();
		           if(type == 1)
		           {
		              System.out.println("Name?");
		              String name = scan.next();
		              System.out.println("Price?");
		              double price = scan.nextDouble();
		              boolean validDate=false;
		              while (validDate == false) {
			              System.out.println("Expiration date?");
			              String date = scan.next();
			              char slash1 = date.charAt(2);
			              char slash2 = date.charAt(5);
			              if (slash1=='/' && slash2=='/'){
			      				validDate = true;}
			              else {System.out.println("Invalid date, try again");}
		              }
			              //CHECK if the expiration is correct 
			              //if not reject new product
			              Item newItem = new Produce(name, price, date);
			              addItem(newItem);
					}
		           else if(type == 2)
		           {
		              System.out.println("Name?");
		              String name = scan.next();
		              System.out.println("Price?");
		              double price = scan.nextDouble();
		              Item newItem = new Shelved(name, price);
		              addItem(newItem);
		           }
		           else if(type == 3)
		           {
		              System.out.println("Name?");
		              String name = scan.next();
		              System.out.println("Price?");
		              double price = scan.nextDouble();
		              int age = -1;
		              while (age <0) {
			              System.out.println("Age restriction?");
			              age = scan.nextInt();
			              if (age <0) {
				              System.out.println("Invalid age, try again");
			              }
		              }
		              Item newItem = new Adult(name, price, age);
		              addItem(newItem);
		           }
		           else
		           {
		              System.out.println("Invalid type");
		           }
		        }
		        else if(choice == 2)
		        {
		           System.out.println("What would you like to buy?");
		           String name = scan.next();
		           buyItem(name);
		        }
		        else if(choice == 3)
		        {
		           System.out.println("What are you looking for?");
		           String name = scan.next();
		           searchItem(name);
		        }
		        else if(choice == 4)
		        {
		           System.out.println("What would you like to modify?");
		           String name = scan.next();
		           modifyItem(name);
		        }
		        else if(choice == 5)
		        {
		           showInventory();
		        }
		        else if(choice == 6)
		        {
		           System.out.println("Goodbye! "
		           		+ "Our stock has been updated with "
		           		+ "your selections for next time");
		        }
		        else
		        {
		           System.out.println("Invalid choice");
		        }
		     }
		  }
		  public static void addItem(Item newItem)
		  {
		  }
		  public static Item[] buyItem(String name)
		  {
					Item[] deleting = new Item[10];
					int count = 0;
					for (int i=0; i<data.length; i++) {
						boolean sell = yesNoPrompt("Add " + data[i].getName(), scan);
						if (!sell) {
							if (deleting.length == count) {
								deleting = resize(deleting, deleting.length * 2);
							}
							deleting[count] = data[i];
							count++;
						}
					}
					
					deleting = resize(deleting, count);
					
					return null;
				}
		  private static Item[] resize(Item[] deleting, int count) {
			// TODO Auto-generated method stub
			return null;
		}
		}
