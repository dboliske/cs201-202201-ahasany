package project;


import java.io.File;

import java.io.FileInputStream;

import java.io.FileOutputStream;

import java.io.IOException;

import java.io.ObjectInputStream;

import java.io.ObjectOutputStream;

public class ShoppingCart  {

      public static void main(String[] args) {

            new ShoppingCart();

      }

      // private helper method to save data to binary file after each operation

      private void save() {

            try {

                  // opening file named cart.dat and storing contents of model

                  FileOutputStream fos = new FileOutputStream(new File(

                              "cart.dat"));

                  ObjectOutputStream ous = new ObjectOutputStream(fos);

                  ous.writeObject(model);

                  ous.close();

                  fos.close();

            } catch (IOException e) {

            }

      }

      // private helper method to load data into the program

      private void load() {

            try {

                  // opening groceries.dat and loading model from it

                  FileInputStream fos = new FileInputStream(new File("groceries.dat"));

                  ObjectInputStream ous = new ObjectInputStream(fos);

                  model = (DefaultListModel) ous.readObject();

                  output.setModel(model);

                  ous.close();

                  fos.close();

            } catch (Exception e) {

            }

      }

      @Override

      public void actionPerformed(ActionEvent ev) {

            // handling button clicks

            if (ev.getSource().equals(addBtn)) {

                  // getting text, adding to model if it is not empty

                  String item = input.getText();

                  if (item.length() == 0) {

                        JOptionPane.showMessageDialog(this, "Item cannot be blank");

                  } else {

                        model.addElement(item);

                  }

            } else if (ev.getSource().equals(remBtn)) {

                  // getting selected items,removing one by one if not empty

                  Object objects[] = output.getSelectedValues();

                  if (objects == null || objects.length == 0) {

                        JOptionPane.showMessageDialog(this, "No item is selected!");

                  } else {

                        for (Object ob : objects) {

                              model.removeElement(ob);

                        }

                  }

            } else {

                  //clearing text boxes

                  model.clear();

                  input.setText("");

            }

            //saving to file.

            save();

      }

}