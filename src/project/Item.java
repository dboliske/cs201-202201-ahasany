package project;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Item {
	//attributes
	  public String name;
	  public double price;
	  public String date;
	  //constructors
	  public void item(String name, double price, String date)
	  {
	     this.name = name;
	     this.price = price;
	     this.date=date;
	  }
	  //methods
	  public String getName()
	  {
	     return name;
	  }
	  public double getPrice()
	  {
	     return price;
	  }
	  public void setName(String name)
	  {
	     this.name = name;
	  }
	  public void setPrice(double price)
	  {
	     this.price = price;
	  }
	
	}

}
